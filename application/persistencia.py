import os
from urllib import parse

from peewee import SqliteDatabase, Model, CharField, BooleanField, PostgresqlDatabase


def get_database():
    """Return local or remote database depending on environment config."""
    REMOTE_DB_FROM_HEROKU = os.environ.get('DATABASE_URL')
    if REMOTE_DB_FROM_HEROKU:
        # Desarmo el string de conexión en cada parametro para inicializar
        # Con argumentos como pide peewee
        parse.uses_netloc.append("postgres")
        print("ANTES: ", REMOTE_DB_FROM_HEROKU)
        url = parse.urlparse(REMOTE_DB_FROM_HEROKU)
        config = {
            "database": url.path[1:],  # Strip leading /
            "user": url.username,
            "password": url.password,
            "host": url.hostname,
            "port": url.port
        }
        print("DESPUES: ", config)
        return PostgresqlDatabase(**config)
    else:
        return SqliteDatabase('personas.sqlite')


db = get_database()


class BaseModel(Model):
    class Meta:
        database = db


class Persona(BaseModel):
    nombre = CharField()
    apellido = CharField()
    mayor_de_edad = BooleanField()


def crear_tabla():
    db.create_tables([Persona])
    print("Tablas creadas ✨")


def ejecutar_prueba():
    # Me conecto a la db y creo las tablas
    db.connect()
    db.create_tables([Persona])

    # Creo personas
    alguien = Persona(nombre='Juana', apellido='De Arco', mayor_de_edad=True)
    otro = Persona(nombre='Carlitos', apellido='Suarez', mayor_de_edad=False)

    # Las guardo en la base de datos
    alguien.save()
    otro.save()

    # Veo todos las personas guardadas
    print("Personas al inicio del día:")
    personas = Persona.select()
    for persona in personas:
        print('>', persona.nombre)

    # Borro una persona
    otro.delete_instance()

    # Y veo que ya no existe
    print("Personas al final del día:")
    personas = Persona.select()
    for persona in personas:
        print('>', persona.nombre)

    # Borro todos las personas de la tabla
    Persona.delete().execute()
    db.close()


if __name__ == '__main__':
    crear_tabla()
