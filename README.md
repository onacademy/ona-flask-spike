# RestApi

Proyecto de ejemplo para aprender sobre APIs REST, HTTP, y serialización JSON.

## Prerequisitos
Python3.6 o superior

[httpie](https://httpie.io/) 
(Se puede instalar con `sudo apt install httpie`) para hacer requests a nuestra api

## Comenzando
1. Crear un entorno virtual aislado (`virtualenv`) para nuestro proyecto 
```
python3.7 -m venv env
# Activar el entorno virtual para entrar a 'tierra segura'
source env/bin/activate
```
2. Instalar las dependencias del proyecto en el entorno aislado.
```
pip install -r requirements.txt
```
3. Crear las tablas para persistir los datos
```
./crear_tablas.sh
```
y deberíamos ver el mensaje `Tablas creadas ✨`


4. Iniciar nuestra api
```
./start.sh
```

## Funcionalidades
1. Leer parametros por get
2. Leer parametros por post (json)
3. Leer headers del request
4. Responder un json seteando el content type
5. Persistir datos y leerlos luego de reiniciar la api


## Conceptos
- API
- REST
- HTTP
- GET/POST
- JSON y formatos de serialización
- Persistencia


## Chequeo de Calidad
Luego de introducir algún cambio, podés asegurarte que todo siga andando corriendo
los chequeos de calidad
```bash
./build.sh
```


## Persistencia
Para persistir los datos en nuestra aplicación vamos a usar [peewee](http://docs.peewee-orm.com/en/latest/peewee/installation.html)

Mientras desarrollamos vamos a usar una base de datos local basada en una archivo,
pero en producción, usaremos una base de datos más robusta.

La tarea de elegir la base de datos según el entorno va a surgir de la función `get_database`
que chequea si estamos corriendo en heroku inspeccionando la variable de entorno `DATABASE_URL`