#!/bin/bash
set -e

echo "🖌️  Running flake8.."
python3 -m flake8 application/ tests/ --max-line-length=119
echo "🧪 Running tests.."
python3 -m pytest
echo "✔️ Checks passed ✨"
