from application.api import app
import json

from application.persistencia import Persona


def test_inicio():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert response.data == b'Hola, Mundo!'


def test_post_personas():
    response = app.test_client().post(
        '/personas',
        data=json.dumps({'persona': {'nombre': 'Nahuel', 'apellido': 'Ambrosini', 'mayor_de_edad': True}}),
        content_type='application/json',
    )

    assert response.status_code == 201
    assert response.json == {
        'personas': [{
            'id': 1,
            'nombre': 'Nahuel',
            'apellido': 'Ambrosini',
            'mayor_de_edad': True
        }]
    }
    # Por último, borro las personas creadas para no dejar side-effects
    # Esto debería hacerse con un fixture de pytest. Pero por simplicidad
    # Se eligió esta alternativa.
    Persona.delete().execute()


def test_leer_headers():
    # Completar
    pass
